using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using SimpleLambda;

namespace SimpleLambda.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestToUpperFunction()
        {

            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            //var upperCase = function.FunctionHandler("hello world", context);

            //Assert.Equal("HELLO WORLD", upperCase);

            Environment.SetEnvironmentVariable("championProductsDBConnectionString", "server=testinstance-dev.cutsj0t1316k.us-east-1.rds.amazonaws.com;Database=ChampionProducts;UID=administrator;PWD=******;");

            var success = function.FunctionHandler(context);

            Assert.True(success);
        }
    }
}
